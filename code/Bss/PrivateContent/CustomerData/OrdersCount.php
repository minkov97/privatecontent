<?php
declare(strict_types=1);

namespace Bss\PrivateContent\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Framework\DataObject;
use Magento\Customer\Model\Session;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * Example data source
 */
class OrdersCount extends DataObject implements SectionSourceInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var OrderCollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * Data constructor.
     * @param Session $session
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param array $data
     */
    public function __construct(
        Session $session,
        OrderCollectionFactory $orderCollectionFactory,
        array $data = []
    ) {
        $this->session = $session;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($data);
    }

    /**
     * @return array
     */
    public function getSectionData()
    {
        return [
            'ordersCount' => $this->getOrdersCount()
        ];
    }

    /**
     * @return int
     */
    protected function getOrdersCount()
    {
        $customerId = $this->session->getCustomer()->getId();
        if ($customerId) {
            return $this->orderCollectionFactory->create()
                ->addFieldToFilter("customer_id", $customerId)->count();
        }
        return 0;
    }
}
