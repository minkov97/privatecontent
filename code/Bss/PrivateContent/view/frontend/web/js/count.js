define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        initObservable: function () {
            this._super().observe(['ordersCount']);
            return this;
        },

        initialize: function () {
            this._super();
            var self = this;
            customerData.reload(['customerOrdersCount']);
            customerData.get('customerOrdersCount').subscribe(function (result) {
                console.log(result);
                self.ordersCount(result.ordersCount);
            });
        }
    });
});
