<?php
declare(strict_types=1);
namespace Bss\PrivateContent\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

class OrdersCount extends Template
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var OrderCollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * OrdersCount constructor.
     * @param Session $session
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Session $session,
        OrderCollectionFactory $orderCollectionFactory,
        Template\Context $context,
        array $data = []
    ) {
        $this->session = $session;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getOrdersCount()
    {
        $customerId = $this->session->getCustomer()->getId();

        if ($customerId) {
            return $this->orderCollectionFactory->create()
                ->addFieldToFilter("customer_id", $customerId)->count();
        }
        return 0;
    }
}
